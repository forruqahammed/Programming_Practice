package com.algoritm.searching;

public class BinarySearch {
	
	public static void main(String[] args) {
		  int arr[] = {10,20,30,40,50};  
	      int key = 40;  
	      int last=arr.length-1;  
		BinarySearch obj = new BinarySearch();
		System.out.print(obj.binarySearch(arr, 0, last, key));
		
	}
	
	
	public int binarySearch(int[] array, int low, int high, int searchKey) {
		
		while(low <= high) {
			int mid = (low + high) / 2;
			if(array[mid] < searchKey) {
				low = mid + 1;
			} else if(array[mid] == searchKey) {
				System.out.println("mid is " + mid);
				return mid;
			} else {
				high = mid -1;
			}
		}
		if(high < low) {
			return -1;
		}
		
		return 0;
	}
	
	

}
