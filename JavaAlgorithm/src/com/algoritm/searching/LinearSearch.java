package com.algoritm.searching;

public class LinearSearch {
	
	public static void main(String[] args) {
		System.out.println("Linear search");
		  int[] array= {10,20,30,50,70,90};    
	      int key = 70; 
		System.out.print("Result of this search is " + linearSearch(array, key));
	}
	
	public static int linearSearch(int[] array, int searchKey) {
		
		for (int i = 0; i < array.length; i++) {
			if(array[i] == searchKey) {
				return i;
			}
		}
		
		return -1;
	}

}
