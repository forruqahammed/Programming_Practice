package com.practice.leader;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
public class Dominator {
	
	public static void main(String[] args) {
		Dominator obj = new Dominator();
		System.out.print(obj.solution4(new int[] {3, 4, 3, 2, 3, -1, 3, 3}));
	}

	public int solution4(int[] A) {
		Map<Integer, Integer> count = new HashMap<>();
		int length = A.length;
		int index = 0;
		for(int a : A) {
			count.put(a, count.getOrDefault(a, 0) + 1);
			if(count.get(a) > length/2) {
				System.out.println(count);
				return index;
			} else {
				index++;
			}	
		}
		return -1;
	}
	
	//Got 100% Scrore from codility Test
	public int solution(int[] A) {
	      
	     Map<Integer, Integer> count = new HashMap<>();
	        
	        int len = A.length;
	        int index = 0;
	        for (int a : A) {
	            count.put(a, count.getOrDefault(a, 0) + 1);
	            
	            if (count.get(a) > len/2) return index;
	            index++;
	        }
	        
	        return -1;
	    }
	
	
	//Got 45% from codility Test
	
	public int solution2(int[] A) {
		if(A.length == 0){
            return -1;
        } else if(A.length < 3) {
        	return 0;
        }

        List<Integer> set = new ArrayList<Integer>();
        
        

        for(int i = 0; i < A.length; i ++){
            set.add(A[i]);
        }

        Arrays.sort(A);
        int count = 1;
        int maxNumber = 0;

        for(int i = 0; i < A.length-1; i++){
            if(A[i] == A[i+ 1]){
                count ++; 
                
            } else {
                if(count > A.length /2 ){
                    maxNumber = A[i];
                    return set.indexOf(maxNumber);
                }
            }
        }
        return -1;
    }
	
	
	
//	public int solution3(int[] A) {
//        int count = 1;
//        int result = -1;
//
//        if(A.length < 3){
//            return result;
//        }
//
//       Arrays.sort(A);
//
//        for(int i = 0; i < A.length -1 ; i++){
//           if(A[i] == A[i + 1]){
//               count ++;
//               
//           } else {
//                if(count > A.length/2){
//                   result = i;
//               }
//               count = 1;
//           }
//
//        }
//        return result;
//    }

}
