package com.practice.sieveoferatosthenes;

public class CountNonDivisible {

	// Got 100 % Score from Codility Test
	public int[] solution3(int[] A) {
		int N = A.length;
		int[] cnt = new int[2 * N + 1];
		for (int a : A) {
			cnt[a]++;
		}
		int[] divisible = new int[cnt.length];
		for (int i = 1; i * i < divisible.length; i++) {
			for (int j = i * i; j < divisible.length; j += i) {
				divisible[j] += cnt[i];
				if (j != i * i) {
					divisible[j] += cnt[j / i];
				}
			}
		}
		int[] result = new int[N];
		for (int i = 0; i < N; i++) {
			result[i] = N - divisible[A[i]];
		}
		return result;
	}

	// Got 55% Score from codility Test
	public int[] solution2(int[] A) {
		int[] arr = new int[A.length];
		for (int i = 0; i < A.length; i++) {
			int count = 0;
			for (int j = 0; j < A.length; j++) {
				if (A[i] % A[j] != 0) {
					count++;
				}
				arr[i] = count;
			}
		}
		return arr;
	}

}
