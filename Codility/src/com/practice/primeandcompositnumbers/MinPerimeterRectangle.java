package com.practice.primeandcompositnumbers;

public class MinPerimeterRectangle {

	public static void main(String[] args) {
		MinPerimeterRectangle obj = new MinPerimeterRectangle();
		System.out.println(obj.solution2(30));
	}

	//Got 100% Score From Codility Test
	public int solution(int N) {
		int square = (int) Math.sqrt(N);
		int minimalPerimeter = Integer.MAX_VALUE;

		for (int i = 1; i <= square; i++) {
			if (N % i == 0) {
				int side = N / i;
				int perimeter = (2 * (i + side));
				minimalPerimeter = Math.min(perimeter, minimalPerimeter);
			}
		}

		return minimalPerimeter;
	}

	// Got 100% Score from Codility Test
	public int solution2(int N) {
		int min = Integer.MAX_VALUE;
		int squareNumber = (int) Math.sqrt(N);

		for (int i = 1; i <= squareNumber; i++) {
			if (N % i == 0) {
				int result = N / i;
				min = Math.min(2 * ((N / i) + i), min);
			}
		}

		return min;
	}

}
