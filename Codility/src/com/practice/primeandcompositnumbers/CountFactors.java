package com.practice.primeandcompositnumbers;

import java.util.HashSet;
import java.util.Set;

public class CountFactors {

	public static void main(String[] args) {
		CountFactors obj = new CountFactors();
		System.out.print(obj.solution(16));
	}

	// Got 100% Score From Codility Test
	public int solution(int N) {
		if (N == 1) {
			return N;
		}
		int square = (int) Math.sqrt(N);
		int count = 0;

		for (int i = 1; i <= square; i++) {
			if (N % i == 0) {
				count += 2;
			}
		}
		double isSuqare = (double) N / square;

		return isSuqare == (double) square ? count - 1 : count;
	}

	// Got 100% Score From Codility test
	public int solution2(int N) {
		int factor = 0;
		int squareNumber = (int) Math.sqrt(N);

		for (int i = 1; i <= squareNumber; i++) {
			if (N % i == 0) {
				factor += 2;
			}
		}
		if (squareNumber * squareNumber == N) {
			factor -= 1;
		}
		return factor;
	}

	// Got 85% Score from codility Test
	public int solution3(int N) {
		if (N == 1) {
			return N;
		}
		Set<Integer> set = new HashSet<>();
		for (int i = 1; i < N; i++) {
			if (N % i == 0) {
				if (set.contains((Integer) i)) {
					break;
				} else {
					set.add(i);
					set.add(N / i);
				}
			}
		}
		return set.size();
	}

	// Got 71% from codility Test
	public int solution4(int N) {
		int maxLength = N / 2;
		int count = 1;
		System.out.println(maxLength);

		for (int i = 1; i < N; i++) {
			if (N % i == 0) {
				count++;
				if (i == maxLength) {
					break;
				}
			}
		}

		return count;
	}
}
