package com.practice.primeandcompositnumbers;

import java.util.ArrayList;

public class Flags {
	
	public int solution(int[] A) {
        ArrayList<Integer> list = new ArrayList<Integer>();  
		for (int i = 1; i < A.length - 1; i++) {
			if (A[i - 1] < A[i] && A[i + 1] < A[i]) {  
				list.add(i);  
			}  
		}  
	    if (list.size() == 1 || list.size() == 0) {  
			return list.size();  
	    }  
        
        int sf = 1;  
        int ef = list.size();  
        int result = 1;  
        while (sf <= ef) {  
            int flag = (sf + ef) / 2;  
            boolean suc = false;  
            int used = 0;  
            int mark = list.get(0);  
            for (int i = 0; i < list.size(); i++) {  
                if (list.get(i) >= mark) {  
                    used++;  
                    mark = list.get(i) + flag;  
					if (used == flag) {                       
						suc = true;  
						break;  
					}  
                }  
            }  
            if (suc) {  
                result = flag;  
                sf = flag + 1;  
            }else {  
                ef = flag - 1;  
            }  
        }  
       return result;  
    }

}
