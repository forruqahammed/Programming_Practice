package com.practice.iteration;

public class BinaryGapPractice {
	
	public static void main(String[] args) {
		System.out.println(solution(38976));
		System.out.println(solution2(9));
		getBinary();
		
	}
	
	public static int solution(int N) {
		String binary = Integer.toBinaryString(N);
		System.out.println(binary);
		
		boolean started = false;
		int counter = 0;
		int maxCount = 0;
		
		for(int i = 0; i<binary.length(); i++) {
			String c = binary.substring(i, i+1);
			if(c.equals("1")) {
				if(started) {
					if(counter> maxCount) {
						maxCount = counter;
					}
				}
				counter = 0;
				started = true;
			}else {
				counter++;
			}

		}
		
		return maxCount;
	}
	
	
	public static void getBinary() {
		int foo = Integer.parseInt("1001100001000000", 2);
		System.out.println(foo);
	}
	
	public static int solution2(int N) {
		
		String binaryString = Integer.toBinaryString(N);
		int counter = 0;
		int maxGap = 0;
		
		for (int i = 0; i < binaryString.length(); i++) {
			if(binaryString.charAt(i) == '0') {
				counter += 1;
			} else {
				if(counter != 0 && maxGap < counter) {
					maxGap = counter;
				}
				counter = 0;
			}
		}
		return maxGap;
		
	}

}
