package com.practice.iteration;

import java.util.Scanner;

public class CyclicRotation {
	
	public static void main(String[] args) {
		int[] A = { 3, 8, 9, 7, 6};
		
		Scanner sc = new Scanner(System.in);
		System.out.println("Please Enter a Iteration Number");
		
		int K = sc.nextInt();
		
		int[] result = solution(A, K);
		
		for(int i : result) {
			System.out.println(i);
		}
		
	}
	
	
	public static int[] solution(int[] A, int K) {
		
		if(A.length == 0 || A.length == K) {
			return A;
		}
		for(int i = 0; i<K; i++) {
			int lastValue = A[A.length-1];
			for(int j = A.length -2; j>= 0; j--) {
				A[j+1] = A[j];
			}
			A[0] = lastValue;
		}
		return A;
		
	}
	

}
