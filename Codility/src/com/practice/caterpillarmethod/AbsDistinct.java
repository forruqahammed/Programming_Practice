package com.practice.caterpillarmethod;

import java.util.HashSet;
import java.util.Set;

public class AbsDistinct {
	
	//Got 100% Score From Codility Test
	public int solution(int[] A) {
		Set<Integer> set = new HashSet<>();

		for (int data : A) {
			if (!set.contains((Integer) Math.abs(data))) {
				set.add(Math.abs(data));
			}
		}

		return set.size();
	}

}
