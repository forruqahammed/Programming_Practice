package com.practice.caterpillarmethod;

import java.util.HashSet;
import java.util.Set;

public class CountDistinctSlices {
	
	public static void main(String[] args) {
		CountDistinctSlices obj = new CountDistinctSlices();
		System.out.print(obj.solution(0, new int[] {0}));
	}
	
	
	 public int solution(int M, int[] A) {
         Set<Integer> set = new HashSet<>();
       for(int data : A){
           if(!set.contains((Integer) data)){
               set.add(data);
           }
       }
        int  arrLength = A.length;
        int setSize = set.size();
       return  ((setSize * 2) + (arrLength-setSize));
   }

}
