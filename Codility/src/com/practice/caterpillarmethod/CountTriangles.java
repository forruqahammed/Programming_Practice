package com.practice.caterpillarmethod;

import java.util.Arrays;

public class CountTriangles {

	public static void main(String[] args) {
		CountTriangles obj = new CountTriangles();
		System.out.print(obj.solution2(new int[] { 10, 2, 5, 1, 8, 12 }));
	}

	// Got 100% Score from Codility Test
	 int solution(int[] A) {
		int count = 0;
		Arrays.sort(A);
		for (int i = 0; i < A.length - 2; i++) {
			int first = i + 1;
			int last = i + 2;
			while (first < A.length - 1) {
				if (last < A.length && A[i] + A[first] > A[last]) {
					last++;
//					System.out.println(last + "    this is last ");
				} else {
					count = count + (last - first - 1);
					first++;
					System.out.println(count + "    this is Count " + last + " last " + first + " first");
				}
			}
		}
		return count;
	}

	 int solution2(int[] A) {
		int n = A.length;

		Arrays.sort(A);

		int count = 0;

		for (int i = n - 1; i >= 1; i--) {
			int l = 0, r = i - 1;
			while (l < r) {
				if (A[l] + A[r] > A[i]) {

					// If it is possible with a[l], a[r]
					// and a[i] then it is also possible
					// with a[l+1]..a[r-1], a[r] and a[i]
					count += r - l;

					// checking for more possible solutions
					r--;
				} else // if not possible check for
				// higher values of arr[l]
				{
					l++;
				}
			}
		}
		return count;
	}

	// Got 75% Scrore from Codility Test
	 int solution3(int arr[]) {
		int count = 0;
		for (int i = 0; i < arr.length; i++) {
			for (int j = i + 1; j < arr.length; j++) {
				for (int k = j + 1; k < arr.length; k++) {
					if (arr[i] + arr[j] > arr[k] && arr[i] + arr[k] > arr[j] && arr[k] + arr[j] > arr[i])
						count++;
				}
			}
		}
		return count;
	}

}
