package com.practice.prefixsums;

import java.util.Arrays;

public class GenomicRangeQuery {

	public static void main(String[] args) {
		GenomicRangeQuery obj = new GenomicRangeQuery();

		int  P[] = {2, 5, 0};   
		int Q[] = {4, 5, 6};
				    	String	S = "CAGCCTA";
//				    String	S = "GGTGCTGG";
		
		int[] result = obj.solution(S, P, Q);
		
		for(int r : result){
			System.out.println(r);
		}
	}
	
	
	
	//Got 25% Score from codility Test
	public int[] solution(String S, int[] P, int[] Q) {
		// write your code in Java SE 8
		int[] array = new int[P.length];

		int[] string = new int[S.length()];

		for (int i = 0; i < S.length(); i++) {
			switch (S.charAt(i)) {
			case 'A':
				string[i] = 1;
				continue;
			case 'C':
				string[i] = 2;
				continue;
			case 'G':
				string[i] = 3;
				continue;
			default:
				string[i] = 4;
			}
		}
		
		
		for (int i = 0; i < P.length; i++) {
			
			array[i] = Math.min(string[P[i]], string[Q[i]]);
		}
		
		Arrays.sort(string);

			if(string[0] > 1) {
				for(int i = 0; i < array.length; i ++) {
					string[i] = string[i] - (string[0] - 1);
				}
			}

		
		return array;
	}

	//Got 37% Score From Codility Test
	public int[] solution2(String S, int[] P, int[] Q) {
		// write your code in Java SE 8
		int[] array = new int[P.length];

		int[] string = new int[S.length()];

		for (int i = 0; i < S.length(); i++) {
			switch (S.charAt(i)) {
			case 'A':
				string[i] = 1;
				continue;
			case 'C':
				string[i] = 2;
				continue;
			case 'G':
				string[i] = 3;
				continue;
			default:
				string[i] = 4;
			}
		}

		for (int i = 0; i < P.length; i++) {
			array[i] = Math.min(string[P[i]], string[Q[i]]);
		}
		
		return array;
	}

}
