package com.practice.prefixsums;

public class PrefixSum {
	
	//Got 100% Score from Codility Test
	public int solution(int[] A) {
		int count = 0;
		int result = 0;
		for (int i = 0; i < A.length; i++) {
			if (A[i] == 1) {
				count++;
			}
		}
		for (int a : A) {
			if (a == 1) {
				count--;
			} else {
				result += count;
			}
		}

		if (result > 1000000000 || result < 0) {
			return -1;
		}
		return result;
	}
}
