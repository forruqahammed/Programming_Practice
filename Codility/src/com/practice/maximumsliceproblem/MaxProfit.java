package com.practice.maximumsliceproblem;

public class MaxProfit {

	public static void main(String[] args) {
		MaxProfit obj = new MaxProfit();
//		System.out.println(obj.solution(new int[] { 80, 41, 85, 87, 74, 95, 56 }));
		System.out.println(obj.solution(new int[] { 23171, 21011, 21123, 21366, 21013, 21367,}));
	}
	  
	

	public int solution(int[] A) {
		if (A.length <= 1) {
			return 0;
		}
		int minPrice = A[0];
		int maxProfit = 0;

		for (int i = 1; i < A.length; i++) {
			if (A[i] < minPrice) {
				minPrice = A[i];
			} else {
				int curProfit = A[i] - minPrice;
				if (curProfit > maxProfit) {
					maxProfit = curProfit;
				}
			}
		}

		return maxProfit;
	}

	// Got 100% Score on Codility Test
	public int solution2(int[] A) {
		if (A.length < 1) {
			return 0;
		}
		int profit = 0;
		int lastEnding = 0;
		int minPrice = A[0];

		for (int i = 0; i < A.length; i++) {
			lastEnding = Math.max(0, A[i] - minPrice);
			minPrice = Math.min(minPrice, A[i]);
			profit = Math.max(lastEnding, profit);
		}
		return profit;
	}

	// Got 66% Score in Codilty Test
	public int solution3(int[] A) {
		int maxProfit = 0;

		if (A.length < 2) {
			return 0;
		}

		for (int i = 0; i < A.length; i++) {
			for (int j = i + 1; j < A.length; j++) {
				maxProfit = Math.max(maxProfit, (A[j] - A[i]));
			}

		}

		return maxProfit;
	}

	// Got 33% Score On Codility Test
	public int solution4(int[] A) {
		int maxProfit = 0;

		if (A.length < 2) {
			return 0;
		}

		for (int i = 0; i < A.length; i++) {
			for (int j = i + 1; j < A.length; j++) {
				maxProfit = Math.max(maxProfit, (A[j] - A[i]));
			}

		}

		return maxProfit;
	}

}
