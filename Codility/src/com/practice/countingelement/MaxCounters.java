package com.practice.countingelement;

public class MaxCounters {

	public static void main(String[] args) {

		MaxCounters mc = new MaxCounters();
		int[] result = mc.solution2(5, new int[] { 3, 4, 4, 6, 1, 4, 4 });

		for (int res : result) {
			System.out.println(res);
		}

	}

	// Got 100 % Score in Codility Test
	public int[] solution(int N, int[] A) {
		int maxValue = 0;
		int minValue = 0;
		int[] array = new int[N];

		for (int i = 0; i < A.length; i++) {
			int index = A[i] - 1;

			if (index >= N) {
				minValue = maxValue;
			} else {
				array[index] = Math.max(array[index] + 1, minValue + 1);
				maxValue = Math.max(maxValue, array[index]);
			}

		}
		for (int j = 0; j < array.length; j++) {
			array[j] = Math.max(array[j], minValue);
		}

		return array;
	}

	// Got 73% Score in Codility Test

	public int[] solution2(int N, int[] A) {
		// write your code in Java SE 8
		int maxCounter = 0;
		int[] counterArray = new int[N];

		for (int i = 0; i < A.length; i++) {
			if (N >= A[i]) {
				counterArray[A[i] - 1] = counterArray[A[i] - 1] + 1;
				maxCounter = Math.max(maxCounter, counterArray[A[i] - 1]);

			} else {

				for (int j = 0; j < counterArray.length; j++) {
					counterArray[j] = maxCounter;
				}
			}
		}

		return counterArray;
	}

	public int[] solution3(int N, int[] A) {

		int maxValue = 0;
		int minValue = 0;

		int[] counters = new int[N];

		for (int i = 0; i < A.length; i++) {
			int operation = A[i];
			if (operation == N + 1) {
				minValue = maxValue;
			} else {
				operation--;
				counters[operation] = Math.max(counters[operation] + 1, minValue + 1);
				maxValue = Math.max(minValue, counters[operation]);
			}
		}

		for (int i = 0; i < counters.length; i++) {
			counters[i] = Math.max(counters[i], minValue);
		}

		return counters;
	}

}
