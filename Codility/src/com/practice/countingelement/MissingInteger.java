package com.practice.countingelement;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

public class MissingInteger {
	
	
	public static void main(String[] args) {
		
		MissingInteger obj = new MissingInteger();
		System.out.println(obj.solution(new int[] {1, 3, 6, 4, 1, 2}));
		
	}
	
	
	// Got 100% Score from Codility Test
	public int solution(int[] A) {
        // write your code in Java SE 8

		Set<Integer> arraySet = new HashSet<Integer>();
	    Set<Integer> totalSet = new HashSet<Integer>();
	   
	    for(int i=0; i<A.length; i++) {
	      arraySet.add(A[i]); 
	      totalSet.add(i+1);  
	    }
	   
	    for(int current : totalSet) {
	      if(!arraySet.contains(current)) {
	        return current;
	      }
	    }

	    if(totalSet.size() == arraySet.size()) {
	      return totalSet.size() + 1;  
	    }
	   
	    return 1;
    }
	
	
	//Got 33 % From Codility Test
	 public int solution4(int[] A) {
		  Set<Integer> arrayset = new HashSet<Integer>();
	        Set<Integer> totalset = new HashSet<Integer>();
	        for(int a : A){
	            if(a > 0){
	                arrayset.add(a);
	                totalset.add(a + 1);
	            }
	        }
	        if(arrayset.isEmpty()){
	            return 1;
	        }
	        for(Integer data : totalset){
	            if(!arrayset.contains(data)){
	                return data;
	            }
	        }
	         return 1;
		  }
	
	// Got 44 % score in Codility test
	public int solution3(int[] A) {
        // write your code in Java SE 8
        Set<Integer> set = new HashSet<Integer>();
        int minValue =  Integer.MAX_VALUE;
        Arrays.sort(A);
        for(int a : A){
            if(a > 0){
                set.add(a);
                minValue = Math.min(minValue, a);
            }
        }
       
        if(set.isEmpty()){
            return 1;
        } else {
            for(Integer a : set){
                if(a == minValue){
                    minValue ++;
                }
            }
            return minValue;
        }

    }
	
	
	//Got 55 % Score on Codility test
	public int solution2(int[] A) {
        Set<Integer> set = new HashSet<Integer>();
        int minValue = Integer.MAX_VALUE;
        int maxValue = 1;

        for(int i = 0; i < A.length; i++){
            if(A[i] > 0 && A[i] < minValue){
               minValue = A[i];
            }
            maxValue = Math.max(maxValue, A[i]);
            if(A[i]> 0){
                set.add(A[i]);
            }
        }
        if(set.isEmpty()){
            return 1;
        } else {
            while(minValue < maxValue+ 1){
                minValue ++;
                if(set.contains(minValue)){
                   
                } else {
                     return minValue;
                }
            }
        }

        return 0;
    }
	
	//Got 33% Score in codility Test
	public int solution5(int[] A) {
        // write your code in Java SE 8
        Set<Integer> set = new HashSet<Integer>();
        int minValue = Integer.MAX_VALUE;
        for(int i = 0; i < A.length; i++){
            set.add(A[i]);
            if(A[i] > 0 && A[i] < minValue ){
                minValue = A[i];
            }
        }
        for(int i = 0; i < set.size(); i++){
            minValue ++;
            if(set.contains(minValue)){
            } else if(A[i] < 0){
            }
             else {
                return minValue;
            }
        }
    return 1;
    }

}
