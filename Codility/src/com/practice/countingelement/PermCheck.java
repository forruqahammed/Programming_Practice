package com.practice.countingelement;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class PermCheck {

	public static void main(String[] args) {
		int[] A = { 4, 1, 3 };
		PermCheck obj = new PermCheck();
		System.out.println(obj.solution(A));
	}

	//Got 100% in codility Test
	public int solution(int[] A) {
		Arrays.sort(A);
		for (int i = 0; i < A.length; i++) {
			if (A[i] == i + 1) {
				// System.out.println(A[i] + " " + i + 1);
			} else {
				return 0;
			}
		}
		return 1;
	}

	// Got 100 % score From Codility
	public int solution2(int[] A) {

		Set<Integer> arraySet = new HashSet<Integer>();
		Set<Integer> permSet = new HashSet<Integer>();

		for (int i = 0; i < A.length; i++) {
			arraySet.add(A[i]);
			permSet.add(i + 1);
		}
		for (Integer integer : permSet) {
			if (arraySet.contains(integer)) {

			} else {
				return 0;
			}
		}

		return 1;
	}

}
