package com.practice.timecomplexity;

import java.util.ArrayList;
import java.util.List;

public class PermMissingElem {

	public static void main(String[] args) {

		PermMissingElem obj = new PermMissingElem();
		System.out.print(obj.solution(new int[] { 2, 3, 1, 5 }));

	}
	
	//Get 100% Score from Codility
	public int solution(int[] A) {

		int totalSum = 0;
		int arraySum = 0;

		for (int i = 0; i < A.length + 1; i++) {
			totalSum += i + 1;
			if (i < A.length) {
				arraySum += A[i];
			}
		}

		return totalSum - arraySum;
	}

	//Get 50% Score From Codility
	public int solution2(int[] A) {

		List<Integer> list = new ArrayList<Integer>();

		for (int i = 1; i <= A.length; i++) {
			list.add(i);
		}

		for (int i = 0; i < A.length; i++) {
			list.remove((Integer) A[i]);
		}

		return list.get(0);
	}

	

}
