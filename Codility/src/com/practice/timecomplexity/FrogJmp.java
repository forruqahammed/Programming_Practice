package com.practice.timecomplexity;

public class FrogJmp {

	public static void main(String[] args) {

		FrogJmp obj = new FrogJmp();
		System.out.println(obj.solution(10, 85, 30));

	}

	//Got 100% Score From Codility
	public int solution(int X, int Y, int D) {
		double distance = Y - X;
		int jump = (int) Math.ceil(distance / D);
		return jump;
	}
	

	// Get 44% Score from Codility
	public int solution2(int X, int Y, int D) {

		int potition = 0;
		while (X < Y) {
			X = X + D;
			potition++;
		}

		return potition;
	}

	// Get 33% Score from Codility
	public int solution3(int X, int Y, int D) {
		int position = 0;
		do {
			X = X + D;
			position++;
		} while (X < Y);
		return position;
	}
}
