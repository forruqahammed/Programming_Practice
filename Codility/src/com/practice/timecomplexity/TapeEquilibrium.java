package com.practice.timecomplexity;

public class TapeEquilibrium {
	
	public static void main(String[] args) {
		int[] A = {3, 1, 2, 4, 3};
		TapeEquilibrium obj = new TapeEquilibrium();
		System.out.println(obj.solution(A));
		
	}

	public int solution(int[] A) {
		 int tapeSum = 0;
	        int totalSum = 0;
	        int difference = 0;
	        int minimulDifference = Integer.MAX_VALUE;

	        for(int i = 0; i< A.length; i++){
	            totalSum += A[i];
	        }

	        for(int j = 0; j < A.length - 1; j++){
	            tapeSum += A[j];
	            totalSum -= A[j];
	            difference = Math.abs(totalSum - tapeSum);
	            minimulDifference = Math.min(difference, minimulDifference);

	        }

	    return minimulDifference;
	}
}
