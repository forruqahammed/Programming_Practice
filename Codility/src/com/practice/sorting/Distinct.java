package com.practice.sorting;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Distinct {
	
	public static void main(String[] args) {
		Distinct obj = new Distinct();
		System.out.println(obj.solution(new int[] {1, 3, 1, 3, 4}));
		
	}
	
	
	//Got 100% Score in Codility Test
	 public int solution(int[] A) {
	        // write your code in Java SE 8
	        if(A.length == 0){
	            return 0;
	        }
	        int count = 1;
	        Arrays.sort(A);

	        for(int i = 1; i <A.length; i++){
	            if(A[i] == A[i- 1]){

	            } else {
	                count ++;
	            }
	        }
	        return count;
	    }
	 
	 //Got 100% Score in Codility test
	 public int solution2(int[] A) {
	        Set<Integer> list = new HashSet<Integer>();

	        int index = 0;
	        if(A.length == 0){
	            return 0;
	        }

	        for(int i = 0; i < A.length; i++){
	            if(!list.contains(Integer.valueOf(A[i]))){
	                list.add(A[i]);
	            }
	        }
	        return list.size();
	    }
	 

	 //Got 75 % Score in Codility Test
		public int solution3(int[] A) {
			List<Integer> list = new ArrayList<Integer>();

	        int index = 0;
	        if(A.length == 0){
	            return 0;
	        }

	        for(int i = 0; i < A.length; i++){
	            if(!list.contains(Integer.valueOf(A[i]))){
	                list.add(A[i]);
	            }
	        }


	        return list.size();
		}
		
		
}
