package com.practice.sorting;

import java.util.Arrays;

public class MaxProductOfThree {
	
	public static void main(String[] args) {
		MaxProductOfThree obj = new MaxProductOfThree();
		System.out.print(obj.solution(new int[] {5, 2, 4, 9, -1}));
	}
	
	
	//Got 100% Score From codility Test
	public int solution(int[] A) {
		Arrays.sort(A);
		int product1, product2, result = 0;

		product1 = A[0] * A[1] * A[A.length - 1];
		product2 = A[A.length - 1] * A[A.length - 2] * A[A.length - 3];
		result = Math.max(product1, product2);
		return result;
	}

}
