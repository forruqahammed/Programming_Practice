package com.practice.sorting;

import java.util.Arrays;
import java.util.Stack;

public class Triangle {

	public static void main(String[] args) {

		Triangle obj = new Triangle();
		System.out.println(obj.solution(new int[] { 10, 2, 5, 1, 8, 20 }));
	}

	// Got 100% from codility Test
	public int solution(int[] A) {
		if (A.length < 3) {
			return 0;
		}
		Arrays.sort(A);
		for (int i = 0; i < A.length - 2; i++) {
			if (A[i] >= 0 && A[i] > A[i + 2] - A[i + 1]) {
				return 1;
			}
		}

		return 0;
	}

	// Got 93% from codility Test
	public int solution2(int[] A) {
		// write your code in Java SE 8
		if (A.length < 3) {
			return 0;
		}
		Arrays.sort(A);

		for (int i = 0; i < A.length - 2; i++) {
			if (A[i] + A[i + 1] > A[i + 2]) {
				return 1;
			}
		}

		return 0;
	}
	
	
	
}
