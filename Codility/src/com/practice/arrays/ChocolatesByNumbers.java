package com.practice.arrays;

public class ChocolatesByNumbers {

	// Got 100% Score From Codility Test
	public int solution(int N, int M) {
		if (M == 1) {
			return N;
		}
		if (M == N) {
			return 1;
		}
		int a = N, b = M;

		while (b != 0) {
			int temp = b;
			b = a % b;
			a = temp;
		}
		return N / a;
	}

	// Got 75% Score from Codility Test
	public int solution2(int N, int M) {
		int counter = 1;
		int start = 0;
		int value;
		while ((start + M) % N != 0) {
			value = (start + M) % N;
			start = value;
			counter++;
		}
		return counter;
	}

}
