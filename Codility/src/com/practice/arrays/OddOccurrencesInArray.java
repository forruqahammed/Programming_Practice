package com.practice.arrays;

import java.util.HashSet;

public class OddOccurrencesInArray {
	
	HashSet<Integer> aList = new HashSet<Integer>();
	
	public static void main(String[] args) {
		OddOccurrencesInArray obj = new OddOccurrencesInArray();
		System.out.println(obj.solution(new int[] {9, 3, 9, 3, 9, 7, 9}));
	}
	
	
	public int solution(int[] A) {
		
		for(int i = 0; i<A.length; i++) {
			int a = A[i];
			if(aList.contains(a)) {
				aList.remove(new Integer(a));
			} else {
				aList.add(a);
			}
		}
		
		return aList.iterator().next();
	}
	
}
