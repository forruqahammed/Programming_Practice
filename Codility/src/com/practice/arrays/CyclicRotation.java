package com.practice.arrays;


public class CyclicRotation {
	
	public static void main(String[] args) {
		int[] A = { 3, 8, 9, 7, 6};
		int K = 2;
		int[] result = solution3(A, K);
		for(int i : result) {
			System.out.print(i);
		}
		
	}
	
	
	public static int[] solution(int[] A, int K) {
		
		if(A.length == 0 || A.length == K) {
			return A;
		}
		for(int i = 0; i<K; i++) {
			int lastValue = A[A.length-1];
			System.out.println(A.length -2);
			for(int j = A.length -2; j>= 0; j--) {
				A[j+1] = A[j];
			}
			A[0] = lastValue;
		}
		return A;
		
	}
	
	public static int[] solution3(int[] A, int K) {
        // write your code in Java SE 8

        if(A.length == 0 || A.length == K){
            return A;
        }

        for(int i = 0; i < K; i++){
            int shiftedValue = A[A.length -1];
            for(int j = 0; j< A.length - 1; j++){
                A[j+ 1] = A[j];
            }
            A[0] = shiftedValue;
        }

        return A;
    }

}
