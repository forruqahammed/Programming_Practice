package com.practice.stacksandqueues;

import java.util.Stack;

public class Nesting {

	// Got 100% Score from Codility Test
	public int solution(String S) {
		Stack<Character> stack = new Stack<>();

		if (S.isEmpty()) {
			return 1;
		}

		if (S.length() < 2) {
			return 0;
		}

		for (int i = 0; i < S.length(); i++) {
			if (S.charAt(i) == '(') {
				stack.push(S.charAt(i));
			} else {
				if (stack.isEmpty() || stack.peek() != '(') {
					return 0;
				} else {
					stack.pop();
				}
			}
		}
		return stack.size() != 0 ? 0 : 1;
	}

	// Got 100% Score from Codility Test
	public int solution2(String S) {
		Stack<Character> stack = new Stack<Character>();
		for (int i = 0; i < S.length(); i++) {
			switch (S.charAt(i)) {
			case '(':
				stack.push(S.charAt(i));
				break;
			case ')':
				if (stack.isEmpty() || stack.peek() != '(') {
					return 0;
				} else {
					stack.pop();
					break;
				}

			}
		}
		return stack.isEmpty() ? 1 : 0;
	}

}
