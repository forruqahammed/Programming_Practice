package com.practice.stacksandqueues;

import java.util.Stack;

public class StoneWall {

	// Got 100% Score from Codlity test
	public int solution(int[] H) {
		Stack<Integer> stack = new Stack<>();
		int block = 0;

		for (int i = 0; i < H.length; i++) {
			while (!stack.isEmpty() && stack.peek() > H[i]) {
				stack.pop();
			}
			if (stack.isEmpty()) {
				block++;
				stack.push(H[i]);
			} else if (stack.peek() == H[i]) {
			} else if (stack.peek() < H[i]) {
				block++;
				stack.push(H[i]);
			}
		}

		return block;
	}

}
