package com.practice.greedyalgorithms;

public class MaxNonoverlappingSegments {
	
	//Got 100% Score From Codility Test
	public int solution(int[] A, int[] B) {

		int N = A.length;
		if (N <= 1) {
			return N;
		}

		int count = 1;
		int previous = B[0];

		for (int i = 0; i < N; i++) {
			if (A[i] > previous) {
				count++;
				previous = B[i];
			}
		}
		return count;

	}

}
