package com.practice.greedyalgorithms;

public class TieRopes {
	
	
	//Got 100% Score From Codility Test
	public int solution(int K, int[] A) {
	       int count = 0;
	       int length = 0;

	       for(int data : A){
	           length += data;
	           if(length >= K){
	               count ++;
	               length = 0;
	           }
	       }
	       return count;
	    }

}
