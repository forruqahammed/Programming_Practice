package com.data.structure.array;

public class LargestSmallestNumber {
	
	
	// You are given an array of numbers. You need to find smallest and largest numbers in the array.
	
	public static void main(String[] args) {
		solution1(new int[] { 5, 3, 4, 8, 6, 2, 1, 7});
		
	}
	
	public static void solution1(int[] A) {
		int smallestNum = A[0];
		int maximumNum = A[0];
		
		for(int i=  0; i <A.length; i++) {
			if(A[i] < smallestNum) {
				smallestNum = A[i];
			} else if(A[i] > maximumNum) {
				maximumNum = A[i];
			}
		}
		System.out.println("Smalles Number " + smallestNum + "   Maximum Number " + maximumNum);
	}

}
