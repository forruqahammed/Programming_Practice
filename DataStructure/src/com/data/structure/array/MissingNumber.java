package com.data.structure.array;

public class MissingNumber {
	
//	You are given an integer array containing 1 to n but one of the number from 1 to n in the array is missing. 
//	You need to provide optimum solution to find the missing number. Number can not be repeated in the arry.
//	For example:
//		
//	int[] arr1={7,5,6,1,4,2};
//	Missing numner : 3
//	int[] arr2={5,3,1,2};
//	Missing numner : 4
	
	public static void main(String[] args) {
		
		MissingNumber obj = new MissingNumber();
		System.out.print(obj.solution1(new int[] {5, 6, 4, 2, 1}));
		
	}
	
	public int solution1(int[] arr) {
		
		int n=arr.length+1;
        int sum=n*(n+1)/2;
        int restSum=0;
        
        for (int i = 0; i < arr.length; i++) {
            restSum+=arr[i];
        }
        
        int missingNumber=sum-restSum;
        return missingNumber;
		
	}
	

}
