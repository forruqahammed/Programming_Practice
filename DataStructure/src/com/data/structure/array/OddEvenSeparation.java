package com.data.structure.array;

public class OddEvenSeparation {
	public static void main(String[] args) {

		int arr[] = { 12, 17, 70, 15, 22, 65, 21, 90 };

		arr = solution(arr);

		for (int i = 0; i < arr.length; i++) {
			System.out.print(arr[i] + " ");
		}
	}

	public static int[] separateEvenOddNumbers(int arr[]) {
		int left = 0;
		int right = arr.length - 1;
		for (int i = 0; i < arr.length; i++) {

			while (arr[left] % 2 == 0) {
				left++;
			}
			while (arr[right] % 2 == 1) {
				right--;
			}

			if (left < right) {
				int temp = arr[left];
				arr[left] = arr[right];
				arr[right] = temp;
			}
		}
		return arr;
	}

	public static int[] solution(int[] arr) {
		int left = 0;
		int right = arr.length - 1;
		int A[] = new int[arr.length];

		for (int i = 0; i < arr.length; i++) {
			if (arr[i] % 2 == 0) {
				A[right] = arr[i];
				right--;

			} else {
				A[left] = arr[i];
				left++;

			}
		}
		return A;
	}

}
