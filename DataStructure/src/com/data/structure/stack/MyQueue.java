package com.data.structure.stack;

public class MyQueue {
	
	private int capacity;
	int arr[];
	int front;
	int rear;
	int currentSize = 0;
	
	public MyQueue(int size) {
		this.capacity = size;
		front = 0;
		rear = -1;
		arr = new int[this.capacity];
	}
	
	public boolean isFull() {
		if(currentSize == capacity) {
			return true;
		}
		return false;
	}
	
	public boolean isEmpty() {
		if(currentSize == 0) {
			return true;
		}
		return false;
	}
	
	
	public void enqueue(int element) {
		if(isFull()) {
			System.out.println("Queue is Full !");
		}else {
			rear ++;
			if(rear == capacity -1 ) {
				rear = 0;
			}
			arr[rear] = element;
			currentSize++;
			System.out.println(element + " Added to the queue");
		}
	}
	
	public void dequeue() {
		if(isEmpty()) {
			System.out.println("Queue is Empty !");
		} else {
			front ++;
			if(front == capacity - 1) {
				front = 0;
				System.out.println(arr[front-1] + " Removed from Queue");
			} else {
				System.out.println(arr[front-1] + " Removed from Queue");
			}
			currentSize--;
		}
	}
	
	public static void main(String a[]) {
		 
        MyQueue myQueue = new MyQueue(6);
        myQueue.enqueue(1);
        myQueue.dequeue();
        myQueue.enqueue(30);
        myQueue.enqueue(44);
        myQueue.enqueue(32);
        myQueue.dequeue();
        myQueue.enqueue(98);
        myQueue.dequeue();
        myQueue.enqueue(70);
        myQueue.enqueue(22);
        myQueue.dequeue();
        myQueue.enqueue(67);
        myQueue.enqueue(23);
    }

}
